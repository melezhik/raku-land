use DateTime::Relative;
use Test;

{ is DateTime.now.later(|%^args).relative, $^expected, $^expected } for
    days    => +1,  'in a day',
    days    => +3,  'in 3 days',
    days    => -1,  'a day ago',
    days    => -3,  '3 days ago',
    hours   => +1,  'in an hour',
    hours   => +3,  'in 3 hours',
    hours   => -1,  'an hour ago',
    hours   => -3,  '3 hours ago',
    minutes => +1,  'in a min',
    minutes => +3,  'in 3 mins',
    minutes => -1,  'a min ago',
    minutes => -3,  '3 mins ago';

done-testing;
