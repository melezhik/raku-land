use Local::Pager;
use Test;

is "{.first}–{.last} of {.total}", '0–0 of 0', 'total=0'
    with Local::Pager.new;

is "{.first}–{.last} of {.total}", '1–10 of 13', 'total=13'
    with Local::Pager.new :13total;

is "{.first}–{.last} of {.total}", '11–13 of 13', 'total=13, page=2'
    with Local::Pager.new :13total :2page;

with Local::Pager.new :3page :42total :query({ :123foo }) {
    is .prev, '?foo=123&page=2' | '?page=2&foo=123', 'prev';
    is .next, '?foo=123&page=4' | '?page=4&foo=123', 'next';
}

done-testing;
