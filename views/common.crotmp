<:macro page(
    :$description = 'Raku Land is a directory of Raku distributions.',
    :$image       = 'https://raku.land/logo.png',
    :$pager, :$svg, :$title, :$q = '', :$url
)>
<!doctype html>

<html lang=en>

<title>Raku Land<?$title> - <$title></?></title>
<meta property="og:title" content="Raku Land<?$title> - <$title></?>">

<link href=/recent/json rel=alternate type=application/json>

<?$description>
    <meta name=description          content="<$description>">
    <meta property="og:description" content="<$description>">
</?>

<?$image>
    <meta property="og:image" content="<$image>">
    <meta property="og:image:width" content=200>
    <meta property="og:image:height" content=200>
</?>

<?$url>
    <link rel=canonical href="<$url>">
    <meta property="og:url" content="<$url>">
</?>

<meta name=viewport content="width=device-width">

<:part css($css)><@$css><link href="<$_>" rel=stylesheet></@></:>
<:part js($js)><@$js script src="<$_>" type=module></@></:>

<?$pager>
    <?$pager.prev><link rel=prev href="<$pager.prev>"></?>
    <?$pager.next><link rel=next href="<$pager.next>"></?>
</?>

<header>
    <a id=logo href=/ title=Home><&HTML($svg.logo)></a>

    <a class=btn href=/rand title="Random Distribution">
        <&HTML($svg.dice)>
        <span>Rand</span>
    </a>

    <a class=btn href=/stats title="Distribution Statistics">
        <&HTML($svg.graph-up)>
        <span>Stats</span>
    </a>

    <form action=/ id=search>
        <input autocomplete=off autofocus name=q
            placeholder="Search distributions" type=search value="<$q>">
    </form>
</header>

<:body>

<footer>
    <a href=mailto:contact@raku.land>Email</a>
    •
    <a href=//gitlab.com/raku-land/raku-land>Git</a>
    •
    <a href=//web.libera.chat#raku-land>IRC</a>
    •
    <a href=//status.raku.land>Status</a>

<:part built($date)>
    <a href=/build>Built <time datetime="<$date>"><$date.relative></time></a>
</:>
</footer>
</:>

<:sub dists($dists, $svg)>
<ul id=dists>
    <@$dists li>
        <div class="eco <?.tags>with-tags</?> <.eco.lc>" title="<.eco>">
            <.eco.uc>
        </div>
        <header>
            <h2>
                <a href="/<.slug>">
                    <?.name_hl><&HTML(.name_hl)></?><!><.name></!>
                </a>
            </h2>
            <time class=badge datetime="<.date>">
                <&HTML($svg.calendar-fill)>
                <.date.relative>
            </time>
            <?.stars.defined div class=badge title="<.stars> stars">
                <&HTML($svg.star-fill)>
                <.stars>
            </?>
        </header>
        <p>
            <?.description_hl><&HTML(.description_hl)></?>
            <!?.description><.description></?>
        <?.tags div>
            <div class=tags>
                <@tags.keys : $key a href="/tags/<.tags.[$key]>">
                    <?.tags_hl.[$key]>
                        <&HTML(.tags_hl.[$key])>
                    </?><!>
                        <.tags.[$key]>
                    </!>
                </@>
            </div>
        </?>
    </@>
</ul>
</:>

<:sub pagination($pager)>
<nav id=pagination>
    <span>
        <?$pager.prev a href="<$pager.prev>">Prev</?>
    </span>
    <span>
        <$pager.first.comma>⁠–<$pager.last.comma> of <$pager.total.comma>
    </span>
    <span>
        <?$pager.next a href="<$pager.next>">Next</?>
    </span>
</nav>
</:>
