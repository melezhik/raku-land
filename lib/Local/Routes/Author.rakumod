unit module Local::Routes::Author;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;
use Local::Pager;

get sub ($author-id where / ':' /, PosInt :$page = 1) {
    my $author = db.query( q:to/SQL/, $author-id ).hash or return not-found;
        SELECT avatar, date, email, id, name, slug, website
          FROM authors
         WHERE id = $1
    SQL

    my @dists = db.query( q:to/SQL/, $author-id, ( $page - 1 ) * 10 ).hashes;
        SELECT date, description, eco, name, slug, stars, tags,
               COUNT(*) OVER()
          FROM distinct_dists
         WHERE author_id = $1
      ORDER BY name, date DESC
         LIMIT 10 OFFSET $2
    SQL

    my $pager = Local::Pager.new :$page :total(@dists[0]<count> // 0);

    # Falling off the end is a 404, but no results isn't.
    return not-found if !$pager.total && $pager.page > 1;

    template 'author.crotmp', { :$author :@dists, :$pager };
}
