unit module Local::Routes::Dist;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;

# Runtime is probably the most interesting, put that first.
my constant @phases = <runtime build test reverse>;

get sub ($author-id, $name, 'badges', $type where 'downloads' | 'version') {
    my $dist = db.query( q:to/SQL/, $author-id, $name ).hash or return not-found;
        SELECT downloads, eco, version
          FROM distinct_dists
         WHERE author_id = $1 AND name = $2
    SQL

    # Keep in sync with css/common.css.
    my constant %eco-colors = ( :CPAN<#BC4D43> :p6c<#43BC4D> :zef<#4D43BC> );

    my $proc = run './bin/badge',
        $type eq 'downloads' ?? $type            !! $dist<eco>,
        $type eq 'downloads' ?? $dist<downloads> !! $dist<version>,
        $type eq 'downloads' ?? '#aaa'           !! %eco-colors{ $dist<eco> },
        :out;

    content 'image/svg+xml', $proc.out.slurp;
}

get sub ($author-id, $name, $tab? is copy where Any:U | 'badges' | 'changes', :$v) {
    $tab //= 'readme';

    my $dist = db.query( q:to/SQL/, $author-id, $name, $v ).hash;
        SELECT a.avatar  author_avatar,
               a.date    author_date,
               a.id      author_id,
               a.name    author_name,
               a.slug    author_slug,
               a.website author_website,
               d.api,
               d.auth,
               d.changes,
               d.date,
               d.description,
               d.download,
               d.downloads,
               d.eco,
               d.id,
               d.issues,
               d.licenses::text[],
               d.name,
               d.perl,
               d.provides,
               d.pull_requests,
               d.readme,
               d.size,
               d.slug,
               d.stars,
               d.tags,
               d.url,
               d.version
          FROM dists   d
          JOIN authors a ON d.author_id = a.id
         WHERE a.id = $1 AND d.name = $2 AND (d.version = $3 OR $3 IS NULL)
      ORDER BY d.version_parts DESC
         LIMIT 1
    SQL

    return not-found if !$dist || ($tab eq 'changes' && !$dist<changes>);

    $dist<long-name> = join '', $dist<name>, |map {
        .value.defined
            ?? ":{ .key }<{ .value.trans: ['<', '>'] => ['\<', '\>'] }>"
            !! Empty
    }, (:ver($dist<version>)), $dist<auth>:p, $dist<api>:p;

    given $dist<url> {
        when /^ 'https://github.com/' / {
            $dist<issues_url>         = "$_/issues";
            $dist<pull_requests_name> = 'Pull Request';
            $dist<pull_requests_url>  = "$_/pulls";
        }
        when /^ 'https://gitlab.com/' / {
            $dist<issues_url>         = "$_/-/issues";
            $dist<pull_requests_name> = 'Merge Request';
            $dist<pull_requests_url>  = "$_/-/merge_requests";
        }
    }

    # FIXME Reverse deps aren't very exact, they're just name matches.
    #       Don't linkify them yet for that very reason.
    $dist<deps> = db.query( q:to/SQL/, |$dist<id name> ).hashes.classify: *<phase>;
        SELECT CASE "from"  -- Add " (from)" if the dep isn't from Raku.
               WHEN 'Raku' THEN name
                           ELSE name || ' (' || "from" || ')'
                END name, phase::text, path
          FROM deps
         WHERE dist_id = $1
         UNION ALL
        SELECT DISTINCT distinct_dists.name, 'reverse' phase, NULL path
          FROM deps
          JOIN distinct_dists ON dist_id = id
         WHERE deps.name = $2 AND "from" = 'Raku'
      ORDER BY name
    SQL

    $dist<versions> = db.query( q:to/SQL/, |$dist<author_id name> ).hashes;
        SELECT date, version
          FROM dists
         WHERE author_id = $1 AND name = $2
      ORDER BY version_parts DESC
    SQL

    my %author = <avatar date id name slug website>.map:
        { $_ => $dist{"author_$_"}:delete };

    my $url = 'https://raku.land/' ~ $dist<slug> ~ ("/$tab" if $tab ne 'readme');

    template 'dist.crotmp', { :%author :$dist :@phases :$tab :$url };
}
