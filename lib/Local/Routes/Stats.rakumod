unit module Local::Routes::Stats;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;

get -> 'stats' {
    my %stats = db.query(q:to/SQL/).hashes.classify: *<fact>;
        WITH stats AS (
            SELECT eco                     "Ecosystem",
                   perl                    "Raku",
                   EXTRACT(year FROM date) "Year"
              FROM distinct_dists
        ) SELECT *, COUNT(*),
                 TO_CHAR(
                    100.0 * COUNT(*) / (SELECT COUNT(*) FROM stats), 'FM90.0'
                 ) percent,
                 CASE GROUPING("Ecosystem", "Raku", "Year")
                 WHEN B'011'::int THEN 'Ecosystem'
                 WHEN B'101'::int THEN 'Raku'
                 WHEN B'110'::int THEN 'Year'
                  END fact
            FROM stats
        GROUP BY GROUPING SETS ("Ecosystem", "Raku", "Year")
        ORDER BY               ("Ecosystem", "Raku", "Year")
    SQL

    template 'stats.crotmp', { :%stats };
}
