unit module Local::Routes::Build;

use Cro::HTTP::Router;
use Cro::WebApp::Template;

my constant $log-file = '/cache/ingest.log'.IO;

get -> 'build' {
    my $log   = $log-file.slurp;
    my $title = 'Build Log - ' ~ $log-file.modified.DateTime;

    template 'build.crotmp', { :$log, :$title };
}
