unit module Local::Routes;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use JSON::Fast;
use Local::DB;
use MIME::Base64;

template-location 'views/', :compile-all;

&render-template.wrap: -> $template, %topic, :%parts {
    state %svg  = dir("svg").map: { .extension("").basename => .slurp };
    state %dist = slurp("esbuild.json").&from-json<outputs>.map:
        { .value<entryPoint> => '/' ~ .key };

    # Remove any previous CSP header in case we're now printing a 500 to a
    # previously attempted template render.
    $*CRO-ROUTER-RESPONSE.remove-header: 'Content-Security-Policy';

    header 'Content-Security-Policy', join ';',
        "base-uri 'none'",
        "connect-src 'self'",
        "default-src 'none'",
        "frame-ancestors 'none'",
        "img-src data: https:",
        "script-src 'self'",
        "style-src 'self'";

    my $name = ~$template.IO.extension: '';
    callwith $template, %( |%topic, :%svg ), :parts(%( |%parts,
        built => '/cache/ingest.log'.IO.modified.DateTime,
        css   => %dist<<css/common.css "css/$name.css">>:v,
        js    => %dist<<js/common.js   "js/$name.js"  >>:v,
    ));
}

our $application is export = route {
    before {
        # Redirect away trailing slashes (except a literal "/" path).
        my $path = .path;
        redirect $path ~ ("?$_" with .query) if $path ~~ s/ <?after .> \/ $//;
    }

    after {
        # Custom error pages.
        content 'text/html', render-template 'error.crotmp', %(
            :title(.get-response-phrase.subst: 'Server responded with '),
        ) if .status >= 400 && !.has-body;
    }

    # Require all the routes.
    for dir 'lib/Local/Routes', :test(*.ends-with: '.rakumod') {
        require ::("Local::Routes::{.extension('').basename}");
    }

    # TODO brotli & gzip.
    get -> 'dist', *@path {
        header 'Cache-Control', 'max-age=31536000, public, immutable';
        header 'Vary', 'Accept-Encoding';

        static 'dist', @path;
    }

    get { static 'static', @_ }
}
