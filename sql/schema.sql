CREATE EXTENSION citext;
CREATE EXTENSION rum;
CREATE EXTENSION tsm_system_rows;

-- Just enough URL encoding to generate valid slugs.
CREATE FUNCTION slug(VARIADIC parts text[]) RETURNS text AS $$
    SELECT array_to_string(array_agg(replace(replace(replace(replace(part,
        ' ', '%20'), '#', '%23'), '/', '%2f'), '?', '%3f')), '/')
      FROM unnest(parts) AS part
$$ LANGUAGE SQL IMMUTABLE STRICT;

-- https://spdx.org/licenses/
CREATE TYPE license AS ENUM (
    'AGPL-3.0-only', 'AGPL-3.0-or-later', 'Apache-2.0', 'Artistic-1.0-Perl',
    'Artistic-2.0', 'BSD-2-Clause', 'BSD-3-Clause', 'BSL-1.0', 'CC0-1.0',
    'EUPL-1.2', 'GPL-1.0-or-later', 'GPL-2.0-only', 'GPL-2.0-or-later',
    'GPL-3.0-only', 'GPL-3.0-or-later', 'ISC', 'LGPL-2.1-only',
    'LGPL-3.0-only', 'LGPL-3.0-or-later', 'MIT', 'NASA-1.3', 'OFL-1.1',
    'Unlicense', 'WTFPL', 'X11', 'Zlib'
);

CREATE TYPE eco    AS ENUM ('CPAN', 'p6c', 'zef');
CREATE TYPE "from" AS ENUM ('Perl5', 'Raku', 'bin', 'native');
CREATE TYPE perl   AS ENUM ('6', '6.c', '6.d', '6.e');
CREATE TYPE phase  AS ENUM ('build', 'runtime', 'test');

CREATE TABLE authors (
    id      text      NOT NULL PRIMARY KEY,
    date    timestamp,
    email   text,
    name    text      NOT NULL,
    slug    text      NOT NULL GENERATED ALWAYS AS (slug(id)) STORED,
    website text,
    avatar  text      NOT NULL GENERATED ALWAYS AS (
        CASE WHEN id LIKE 'cpan:%'
             THEN '//www.gravatar.com/avatar/'
               || md5(lower(substr(id, 6)) || '@cpan.org') || '?d=identicon'
             WHEN id LIKE 'github:%'
             THEN '//avatars.githubusercontent.com/' || substr(id, 8) || '?'
             ELSE '//www.gravatar.com/avatar/'
               || md5(lower(COALESCE(email, id))) || '?d=identicon'
         END
    ) STORED,
    -- Search on display name and ID without the eco prefix.
    search tsvector NOT NULL GENERATED ALWAYS AS (
        to_tsvector('english', name) ||
        to_tsvector('english', regexp_replace(id, '^[^:]+:', ''))
    ) STORED
);

CREATE TABLE dists (
    date          timestamp NOT NULL,
    id            int       NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    downloads     int,
    eco           eco       NOT NULL,
    issues        int,
    pull_requests int,
    size          int,
    stars         int,
    licenses      license[] NOT NULL,
    perl          perl      NOT NULL,
    api           text,
    auth          text,
    author_id     text      NOT NULL REFERENCES authors(id) ON DELETE CASCADE,
    changes       text,
    description   text,
    download      text,
    name          citext    NOT NULL,
    provides      text[]    NOT NULL,
    readme        text,
    slug          text      NOT NULL GENERATED ALWAYS AS (slug(author_id, name)) STORED,
    tags          text[]    NOT NULL,
    url           text,
    version       text      NOT NULL CHECK (
        version = '*' OR version ~ '^v?\d+(\.\d+)*[a-z]*$'
    ),
    version_parts int[]     NOT NULL GENERATED ALWAYS AS (
        string_to_array(
            regexp_replace(version, '[^0-9.]', '', 'g'), '.'
        )::int[]
    ) STORED,
    UNIQUE (slug, version)
);

CREATE TABLE deps (
    dist_id int    NOT NULL REFERENCES dists(id) ON DELETE CASCADE,
    "from"  "from" NOT NULL,
    phase   phase  NOT NULL,
    api     text,
    auth    text,
    name    text   NOT NULL,
    ver     text,
    path    text   -- Might point to a dist/module, might be versioned.
);

CREATE MATERIALIZED VIEW distinct_dists AS
     SELECT DISTINCT ON (author_id, name) *,
            (   setweight(to_tsvector('english',                 name     ), 'A') ||
                setweight(to_tsvector('english', COALESCE(description, '')), 'B') ||
                setweight(array_to_tsvector(                     tags     ), 'C')
            ) AS search
       FROM dists
   ORDER BY author_id, name, version_parts DESC;

-- Needed to refresh concurrently.
CREATE UNIQUE INDEX distinct_dists_key ON distinct_dists(author_id, name);

-- Used by / (search)
CREATE INDEX        authors_search ON authors        USING rum(search);
CREATE INDEX distinct_dists_search ON distinct_dists USING rum(search);

-- Used by /recent
CREATE INDEX dists_date_name ON dists(date DESC, name);

-- Check the tables are structured optimally.
-- https://www.2ndquadrant.com/en/blog/on-rocks-and-sand/
  SELECT c.relname, a.attname, t.typname, t.typalign, t.typlen
    FROM pg_attribute a
    JOIN pg_class     c ON c.oid = a.attrelid
    JOIN pg_namespace n ON n.oid = c.relnamespace
    JOIN pg_type      t ON t.oid = a.atttypid
   WHERE a.attnum >= 0
     AND c.relkind IN ('m', 'r')
     AND n.nspname = 'public'
ORDER BY c.relname, t.typlen DESC, t.typname, a.attname;

CREATE ROLE "raku-land" WITH LOGIN;

-- Only owners can refresh/vaccum.
ALTER             TABLE authors        OWNER TO "raku-land";
ALTER             TABLE deps           OWNER TO "raku-land";
ALTER             TABLE dists          OWNER TO "raku-land";
ALTER MATERIALIZED VIEW distinct_dists OWNER TO "raku-land";
