use HTTP::Tiny;
use Test;

for <
    200     /               -
    307     //              /
    404     /foo            -
    307     /foo/           /foo
    307     /foo/bar/baz/   /foo/bar/baz
    404     /foo?bar=/      -
    307     /foo/?bar=/     /foo?bar=/
> -> $status, $old-path, $new-path {
    state $ua = HTTP::Tiny.new :!max-redirect;

    my %res = $ua.get: "https://localhost$old-path";

    is %res<status>,            $status,   "[status] $old-path";
    is %res<headers><location>, $new-path, "[header] $old-path"
        if $status == 307;
}

done-testing;
