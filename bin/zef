#!/usr/bin/env raku

use DateTime::Parse;
use HTTP::Tiny;
use JSON::Fast;

constant $host = 'https://360.zef.pm';

# Disable output buffering
.out-buffer = False for $*ERR, $*OUT;

my %dirs = mkdir("/cache/zef").dir.grep(* ne '/cache/zef/index.json').SetHash;

my $ua = HTTP::Tiny.new :throw-exceptions;

$ua.mirror: $host, '/cache/zef/index.json';

for slurp("/cache/zef/index.json").&from-json».<path> {
    my $tar = "/cache/zef/$_.IO.basename()".IO;
    my $dir = $tar.extension("").extension("");

    %dirs{$tar}:delete;

    next if %dirs{$dir}:delete;

    my %res = $ua.mirror: "$host/$_", $tar;

    # What is the minimum number of dirs to strip to get a meta at the root?
    my @metas = qqx{tar -tzf $tar}.lines.grep: *.IO.basename eq 'META6.json';
    my $strip = @metas».split("/", :skip-empty)».elems.min - 1;

    run «tar --strip $strip -C $dir.mkdir() -xzf $tar»;

    $dir.add("date").spurt: DateTime::Parse.new: %res<headers><last-modified>;
    $dir.add("size").spurt: $tar.s;
    $dir.add("download").spurt: "$host/$_";
}

# Delete files that are deleted upstream.
run «rm -rf %dirs.keys()» if %dirs;
