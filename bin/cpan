#!/usr/bin/env raku

# We rsync archives from CPAN into the following directory structure:
#
#   /cache/rsync/J/JJ/JJATRIA/Perl6/App-Lorea-0.0.1.tar.gz
#               /J/JJ/JJATRIA/Perl6/App-Lorea-0.0.2.tar.gz
#               /...
#
# We then extract these tarballs into a slightly flatter structure:
#
#   /cache/cpan/JJATRIA/App-Lorea-0.0.1/...
#              /JJATRIA/App-Lorea-0.0.2/...
#              /...
#
# In both cases we track upstream and delete remnants when upstream does.

# Disable output buffering.
.out-buffer = False for $*ERR, $*OUT;

my %skip = (
    'ANDINUS/octans-0.2.1',         # Invalid JSON
    'ARNE/Game-Amazing-0.9.0',      # Invalid JSON
    'DMAESTRO/Seq-Bounded-1.0.0',   # Invalid license
    'DOOM/Augment-Util-0.0.1',      # Invalid JSON
    'DOOM/Augment-Util-0.0.2',      # Invalid JSON
    'DOOM/Augment-Util-0.0.3',      # Invalid JSON
    'DOOM/Object-Examine-0.0.1',    # Invalid JSON
    'DOOM/Object-Examine-0.0.2',    # Invalid JSON
    'DOOM/Object-Examine-0.0.3',    # Invalid JSON
    'DOOM/Symbol-Scan-0.0.1',       # Invalid JSON
    'DOOM/Symbol-Scan-0.0.2',       # Invalid JSON
    'DOOM/Symbol-Scan-0.0.3',       # Invalid JSON
    'DOOM/method-menu-0.0.1',       # Invalid JSON
    'DOOM/method-menu-0.0.2',       # Invalid JSON
    'DOOM/method-menu-0.0.3',       # Invalid JSON
    'FROGGS/ACME-Ignore-Me-0.008',  # Invalid JSON
    'JDV/Data-Selector-1.00',       # Invalid JSON
    'JGOFF/Perl6-Tidy-y',           # Invalid version
    'JNTHN/cro-http-0.7.6',         # Invalid license
    'JNTHN/cro-http-0.7.6.1',       # No meta
    'PMQS/Archive-SimpleZip-0.3.0', # No meta
).SetHash;

# Exclude https://shadow.cat/blog/matt-s-trout/the-psixdists-experiment/
run <
    rsync --archive --delete-excluded --inplace --prune-empty-dirs
    --exclude /P/PS/PSIXDISTS/Perl6
    --include /*/*/*/Perl6/*.tar.gz
    --include /*/*/*/Perl6/*.tgz
    --include */
    --exclude *
    cpan-rsync.perl.org::CPAN/authors/id/ /cache/rsync
>;

# A set of the currently extracted archives.
my %dirs = mkdir("/cache/cpan").dir».dir.SetHash;

for '/cache/rsync'.IO.map: { .d ?? |.dir».&?BLOCK !! $_ } {
    m/ $<auth> = \w+ '/Perl6/' $<dist> = .+ < .tar.gz .tgz > /;

    next if %skip{"$<auth>/$<dist>"}:delete;

    my $dir = mkdir "/cache/cpan/$<auth>/$<dist>";

    next if %dirs{$dir}:delete;  # Skip if we've already extracted.

    # What is the minimum number of dirs to strip to get a meta at the root?
    my @metas = qqx{tar -tzf $_}.lines.grep:
        *.IO.basename eq 'META6.json' | 'META.json' | 'META.info';
    my $strip = @metas».split("/", :skip-empty)».elems.min - 1;

    $strip = 0 if $strip == Inf;

    # Extract the tarball to dir.
    run «tar --strip $strip -C $dir -xzf $_»;

    # Copy tarball mtime to dir.
    $dir.add("date").spurt: .modified.DateTime;
    $dir.add("size").spurt: .s;
    $dir.add("download").spurt:
        'https://cpan.metacpan.org/authors/id/' ~ .substr: 12;
}

# Delete extractions that are deleted upstream.
run «rm -rf %dirs.keys()» if %dirs;

say "Skips [%skip.keys()] are no longer needed" if %skip;
