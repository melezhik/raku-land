POSTGRES := postgres:13.5-alpine
SHELL    := /bin/bash

export COMPOSE_FILE=docker/docker-compose.yml

# Doesn't actually bump the deps in META6.json but shows what can be upgraded.
bump:
	@docker run --rm registry.gitlab.com/raku-land/raku-land \
	    zef --dry --/test --warn upgrade

cert:
	@mkcert -install localhost
	@chmod +r localhost-key.pem

db:
	@ssh -t root@raku.land sudo -iu postgres psql raku-land

db-dev:
	@docker-compose exec db psql -U postgres raku-land

db-diff:
	@diff --color --label live --label dev --strip-trailing-cr -su    \
	    <(ssh root@raku.land sudo -iu postgres pg_dump -Os raku-land) \
	    <(docker-compose exec -T db pg_dump -OsU postgres raku-land)

dev:
	@touch docker/.env
	@docker-compose rm -f
	@docker-compose up --build

live:
	@./esbuild

	@docker buildx build --pull --push --file docker/app.Dockerfile \
	    --tag registry.gitlab.com/raku-land/raku-land .

	@ssh deploy@raku.land "                                      \
	    docker pull registry.gitlab.com/raku-land/raku-land &&   \
	    docker stop raku-land;                                   \
	    docker rm raku-land;                                     \
	    docker run                                               \
	        --detach                                             \
	        --env-file   /etc/raku-land.env                      \
	        --init                                               \
	        --name       raku-land                               \
	        --network    caddy                                   \
	        --read-only                                          \
	        --restart    always                                  \
	        --tmpfs      /.raku/short                            \
	        --user       nobody:nobody                           \
	        --volume     cache:/cache                            \
	        --volume     /var/run/postgresql:/var/run/postgresql \
	    registry.gitlab.com/raku-land/raku-land &&               \
	    docker system prune -f"

logs:
	@ssh deploy@raku.land docker logs --tail 5 -f raku-land
