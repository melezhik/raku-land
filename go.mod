module gitlab.com/raku-land/raku-land

go 1.19

require (
	github.com/alecthomas/chroma v0.9.2
	github.com/microcosm-cc/bluemonday v1.0.21
	github.com/narqo/go-badge v0.0.0-20220127184443-140af28a266e
	github.com/tdewolff/minify/v2 v2.12.4
	github.com/yuin/goldmark v1.5.2
	github.com/yuin/goldmark-emoji v1.0.1
	github.com/yuin/goldmark-highlighting v0.0.0-20210516132338-9216f9c5aa01
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/tdewolff/parse/v2 v2.6.4 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/net v0.0.0-20221017152216-f25eb7ecb193 // indirect
)
